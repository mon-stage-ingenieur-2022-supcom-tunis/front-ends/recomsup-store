





  $(function() {

    function editObject()
    {
      var text = $('#text').val();
      var fName = 'Arial';
      var fSize = +$('#fontSize').val();
      var diameter = +$('#diameter').val();
      var kerning = +$('#kerning').val();
      var flipped = $('#flip').is(':checked');
      var obj = fcanvas.getActiveObject();

      if (obj) {
        obj.set({
          text: text,
          diameter: +$('#diameter').val(),
          fontSize: fSize,
          fontFamily: fName,
          kerning: kerning,
          flipped: flipped
        });
        fcanvas.renderAll();
      }
      else {
        obj = new fabric.TextCurved(text, {
          diameter: +$('#diameter').val(),
          fontSize: fSize,
          fontFamily: fName,
          kerning: kerning,
          flipped: flipped,
          left: 50,
          top: 50
        });
        fcanvas.add(obj);
      }
    }

    // Create fabric canvas
    fabric.Object.prototype.objectCaching = false;
    var fcanvas = new fabric.Canvas('c');

    // Add a circular text
    fcanvas.add(new fabric.TextCurved('It feels so nice to be able to draw', {
      diameter: 360,
      fontSize: 32,
      fontFamily: 'Arial',
      left: 50,
      top: 50,
      fill: 'red'
    }));

    // And another one
    fcanvas.add(new fabric.TextCurved('any text around a circular path', {
      diameter: 360,
      fontSize: 32,
      fontFamily: 'Arial',
      left: 395,
      top: 405,
      fill: 'black',
      angle: -180
      //flipped: true
    }));

    // Update controls
    function update(e) {
      var obj = e.target;
      $('#diameter').val(obj.diameter);
      $('#text').val(obj.text);
      $('#fontSize').val(obj.fontSize);
      $('#kerning').val(obj.kerning);
      $('#flip').prop('checked', obj.flipped);
    }

    fcanvas.on({
      'object:selected': function(e) {
        update(e);
      },
      'selection:updated': function(e) {
        update(e);
      }
    })

    $('#diameter,#fontSize,#kerning').on('input', editObject);
    $('#text').on('keyup', editObject);
    $('#flip,#newObject').on('click', editObject);
  });
