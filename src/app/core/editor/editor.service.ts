import { Injectable } from '@angular/core';
declare var require:any
const fabric = require('fabric').fabric
declare var FontFace:any;

@Injectable({
  providedIn: 'root'
})
export class EditorService {

  constructor() { }


  Flip(canvas:any,y=false,x=false){
    if(y){
      canvas.getActiveObjects().forEach((item:any)=>{
        if(!item.flipY){
          item.set({flipY:y})
        }else{
          item.set({flipY:!y})

        }
      })
    }

    if(x){
      canvas.getActiveObjects().forEach((item:any)=>{
        if(!item.flipX){
          item.set({flipX:x})
        }else{
          item.set({flipX:!x})

        }
      })
    }
    canvas.requestRenderAll();


  }





  addText(Text:any,canvas:any,path=false){

    let message= Text||'Hello Guy !!'
    var text= new fabric.IText(message,{
      fill:"blue",
      fontSize:25,

    });
    if(path){
    text = this.textPath(text)
    }

    canvas.add(text).setActiveObject(text)
     canvas.renderAll(text);
    canvas.centerObject(text);
    canvas.requestRenderAll(text);
  }
overline(canvas:any){
  let objs:any= canvas.getActiveObjects()
  objs.forEach((element:any) => {
    if(!element.overline){
      element.set({overline:true})
      canvas.requestRenderAll(canvas);

    }else{
      element.set({overline:false})
      canvas.requestRenderAll(canvas);

    }
  });
}

  lineThrought(canvas:any){
    let objs:any= canvas.getActiveObjects()
  objs.forEach((element:any) => {
    if(!element.linethrough){
      element.set({linethrough:true})

    }else{
      element.set({linethrough:false})

    }
  });
  canvas.requestRenderAll();
  }

lock(canvas:any){
  let objs:any= canvas.getActiveObjects()
  objs.forEach((element:any) => {
    if(!element.lockMovmentX){
      element.set({lockMovmentY:true,lockMovmentX:true})
    }else{
      element.set({lockMovmentY:false,lockMovmentX:false})
    }
  });
  canvas.requestRenderAll();
}

textPath(text:any){
  text= text.set({path:new fabric.Path('M 0 0 C 50 -200 150 -200  200  0', {
    strokeWidth: 2,
    visible: false,
  }),
  pathSide: 'left',
  pathStartOffset: 0,
  })
return text
}

  remove(canvas:any){
   let objs:any= canvas.getActiveObjects()

   objs.forEach((element:any) => {
       canvas.remove(element)
   });
   canvas.requestRenderAll();

  }


  setPhoto(canvas:any,url:any){
    fabric.Image.fromURL(url,(img:any) => {
      this.removeWhite(img,canvas)
      canvas.add(img)
      canvas.setActiveObject(img);
      canvas.centerObject(img);
      canvas.renderAll(img);
      canvas.requestRenderAll();

    },
    {
       crossOrigin: "Anonymous",
       scaleX: 0.5,
        scaleY: 0.5,
        angle:-30,
        clipPath: this.ClipPath(canvas),

    }
    );
  }

  removeWhite(object:any,canvas:any){
    var filter = new fabric.Image.filters.RemoveColor({
      threshold: 0.5,
    });
    object.filters.push(filter);
    object.applyFilters();
    canvas.renderAll();
  }


color(canvas:any,value:any){
  for(let i of canvas.getActiveObjects()){

    if(i.type=="i-text"||i.type=='text-curved'){
      i.set({fill:value})
      canvas.renderAll(i)
    }else{
      i._objects.forEach((item:any)=>{
          item.set({fill:value})
         canvas.renderAll(item)
      }

      )

    }
  }
}


bold(canvas:any){
  canvas.getActiveObjects().forEach((el:any)=>{
    if(el.text){
      if(el.fontWeight=="normal"){
        el.set({fontWeight:'bold'});
        canvas.renderAll(el);
        canvas.requestRenderAll()
      }else{
        el.set({fontWeight:'normal'});
        canvas.renderAll(el);
        canvas.requestRenderAll()
    }
    }

})

}

italic(canvas:any){
  canvas.getActiveObjects().forEach((el:any)=>{
    if(el.fontStyle!="normal"){
      el.set({fontStyle:'normal'});
      canvas.renderAll(el)
      canvas.requestRenderAll()
    }else{
      el.set({fontStyle:'italic'});
          canvas.renderAll(el);
      canvas.requestRenderAll()
    }
  })
}

copy=(canvas:any)=>{
  canvas.getActiveObject().clone(function(cloned:any) {
    canvas._clipboard= cloned;
  });
}

toBack(canvas:any){
  canvas.getActiveObjects().forEach((el:any)=>{
  el.bringForward(true)
    canvas.requestRenderAll()
  })
}

paste=(canvas:any)=>{
  canvas._clipboard.clone(function(clonedObj:any) {
    canvas.discardActiveObject();
    clonedObj.set({
      left: clonedObj.left + 15,
      top: clonedObj.top + 15,
      evented: true,
    });
    if (clonedObj.type === 'activeSelection') {
      clonedObj.canvas = canvas;
      clonedObj.forEachObject(function(obj:any) {
        canvas.add(obj);
      });
      clonedObj.setCoords();
    } else {
      canvas.add(clonedObj);
    }
    canvas._clipboard.top += 15;
    canvas._clipboard.left += 15;
    canvas.setActiveObject(clonedObj);
    canvas.requestRenderAll();
  });
}





toFront(canvas:any){
  canvas.getActiveObjects().forEach((el:any)=>{
   // canvas.sendToFront(el)
   el.bringToFront(true)
    canvas.requestRenderAll()
  })
}

centerObject(canvas:any){

canvas.getActiveObjects().forEach((item:any)=>{
       canvas.centerObject(item)
  })
  canvas.renderAll(canvas)

}


groupAll(canvas:any){
  let objs=[]


}

loadSVG(canvas:any,url:any){
fabric.loadSVGFromURL(url,  (
  objects:any,
  options:any,
  elements:any,
  allElements:any
)=>
{
   objects.forEach(function (obj:any, index:any) {
      if (obj["type"] == "image") {
          if (obj["id"].toUpperCase() == "BACKGROUND") {
              fabric.Image.fromURL(obj['xlink:href'], function (img:any) {
                  canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas));
              });
          } else {
              obj.set({
                  selectable: true,
                  evented: true
              });
              canvas.add(obj).renderAll();
          }
      } else if (obj["type"] == "text") {
          var element = elements[index];
          var childrens = [].slice.call(element.childNodes);
          var value = "";
          childrens.forEach(function (el:any, index:any, array:any) {
              if (el.nodeName == "tspan") {
                  value += el.childNodes[0].nodeValue;
              } else if (el.nodeName == "#text") {
                  value += el.nodeValue;
              }

              if (index < childrens.length - 1) {
                  value += "\n";
              }
          });

          value =
              obj["text-transform"] == "uppercase"
                  ? value.toUpperCase()
                  : value;

          var text = new fabric.IText(obj.text, obj.toObject());
          text.set({
              text: value,
              type: 'i-text'
          });

          var left = 0;
          var _textAlign = obj.get("textAnchor")
              ? obj.get("textAnchor")
              : "left";
          switch (_textAlign) {
              case "center":
                  left = obj.left - text.getScaledWidth() / 2;
                  break;
              case "right":
                  left = obj.left - text.getScaledWidth();
                  break;
              default:
                  left = obj.left;
                  break;
          }

          text.set({
              left: left,
              textAlign: _textAlign
          });
          canvas.add(text).renderAll();
      } else {
   canvas.add(obj).renderAll(); }})
  canvas.setWidth(options.width);
  canvas.setHeight(options.height);
  canvas.calcOffset();
})


}


blender(canvas:any,object:any){
  var filter = new fabric.Image.filters.BlendColor({
    color: '#000',
    mode: 'multiply'
   });
   var filter = new fabric.Image.filters.BlendImage({
    image: object,
    mode: 'multiply',
    alpha: 0.5
   });
   object.filters.push(filter);
   object.applyFilters();
   canvas.renderAll();
}


ClipPath(canvas:any,rad=150){
  var radius = rad||200;
  return new fabric.Circle({
    left: 0,
    top: 0,
    radius: radius,
    startAngle: 0,
    angle: Math.PI * 2,
    originX: "top",
  });
}


curved(){

  return (function(fabric) {

    /*
     * TextCurved object for fabric.js
     * @author Arjan Haverkamp (av01d)
     * @date January 2018
     */

    fabric.TextCurved = fabric.util.createClass(fabric.Object, {
      type: 'text-curved',
      diameter: 250,
      kerning: 0,
      text: '',
      flipped: false,
      fill: '#000',
      fontFamily: 'Times New Roman',
      fontSize: 24, // in px
      fontWeight: 'normal',
      fontStyle: '', // "normal", "italic" or "oblique".
       cacheProperties: fabric.Object.prototype.cacheProperties.concat('diameter', 'kerning', 'flipped', 'fill', 'fontFamily', 'fontSize', 'fontWeight', 'fontStyle', 'strokeStyle', 'strokeWidth'),
      strokeStyle: null,
      strokeWidth: 0,

      initialize: function(text:any, options:any) {
        options || (options = {});
        this.text = text;

        this.callSuper('initialize', options);
        this.set('lockUniScaling', true);

        // Draw curved text here initially too, while we need to know the width and height.
        var canvas = this.getCircularText();
        this._trimCanvas(canvas);
        this.set('width', canvas.width);
        this.set('height', canvas.height);
      },

      _getFontDeclaration: function()
      {
        return [
          // node-canvas needs "weight style", while browsers need "style weight"
          (fabric.isLikelyNode ? this.fontWeight : this.fontStyle),
          (fabric.isLikelyNode ? this.fontStyle : this.fontWeight),
          this.fontSize + 'px',
          (fabric.isLikelyNode ? ('"' + this.fontFamily + '"') : this.fontFamily)
        ].join(' ');
      },

      _trimCanvas: function(canvas:any)
      {
        var ctx:any = canvas.getContext('2d'),
          w :any= canvas.width,
          h :any= canvas.height,
          pix:any = {x:[], y:[]}, n,
          imageData = ctx.getImageData(0,0,w,h),
          fn = function(a:any,b:any) { return a-b };

        for (var y:any = 0; y < h; y++) {
          for (var x:any = 0; x < w; x++) {
            if (imageData.data[((y * w + x) * 4)+3] > 0) {
              pix.x.push(x);
              pix.y.push(y);
            }
          }
        }
        pix.x.sort(fn);
        pix.y.sort(fn);
        n = pix.x.length-1;

        w = pix.x[n] - pix.x[0];
        h = pix.y[n] - pix.y[0];
        var cut = ctx.getImageData(pix.x[0], pix.y[0], w, h);

        canvas.width = w;
        canvas.height = h;
        ctx.putImageData(cut, 0, 0);
      },

      // Source: http://jsfiddle.net/rbdszxjv/
      getCircularText: function()
      {
        var text:any = this.text,
          diameter:any = this.diameter,
          flipped :any= this.flipped,
          kerning :any= this.kerning,
          fill:any = this.fill,
          inwardFacing:any = true,
          startAngle:any = 0,
          canvas = fabric.util.createCanvasElement(),
          ctx = canvas.getContext('2d'),
          cw, // character-width
          x, // iterator
          clockwise = -1; // draw clockwise for aligned right. Else Anticlockwise

        if (flipped) {
          startAngle = 180;
          inwardFacing = false;
        }

        startAngle *= Math.PI / 180; // convert to radians

        // Calc heigt of text in selected font:
        var d = document.createElement('div');
        d.style.fontFamily = this.fontFamily;
        d.style.whiteSpace = 'nowrap';
        d.style.fontSize = this.fontSize + 'px';
        d.style.fontWeight = this.fontWeight;
        d.style.fontStyle = this.fontStyle;
        d.textContent = text;
        document.body.appendChild(d);
        var textHeight = d.offsetHeight;
        document.body.removeChild(d);

        canvas.width = canvas.height = diameter;
        ctx.font = this._getFontDeclaration();

        // Reverse letters for center inward.
        if (inwardFacing) {
          text = text.split('').reverse().join('')
        };

        // Setup letters and positioning
        ctx.translate(diameter / 2, diameter / 2); // Move to center
        startAngle += (Math.PI); // Rotate 180 if outward
        ctx.textBaseline = 'middle'; // Ensure we draw in exact center
        ctx.textAlign = 'center'; // Ensure we draw in exact center

        // rotate 50% of total angle for center alignment
        for (x = 0; x < text.length; x++) {
          cw = ctx.measureText(text[x]).width;
          startAngle += ((cw + (x == text.length-1 ? 0 : kerning)) / (diameter / 2 - textHeight)) / 2 * -clockwise;
        }

        // Phew... now rotate into final start position
        ctx.rotate(startAngle);

        // Now for the fun bit: draw, rotate, and repeat
        for (x = 0; x < text.length; x++) {
          cw = ctx.measureText(text[x]).width; // half letter
          // rotate half letter
          ctx.rotate((cw/2) / (diameter / 2 - textHeight) * clockwise);
          // draw the character at "top" or "bottom"
          // depending on inward or outward facing

          // Stroke
          if (this.strokeStyle && this.strokeWidth) {
            ctx.strokeStyle = this.strokeStyle;
            ctx.lineWidth = this.strokeWidth;
            ctx.miterLimit = 2;
            ctx.strokeText(text[x], 0, (inwardFacing ? 1 : -1) * (0 - diameter / 2 + textHeight / 2));
          }

          // Actual text
          ctx.fillStyle = fill;
          ctx.fillText(text[x], 0, (inwardFacing ? 1 : -1) * (0 - diameter / 2 + textHeight / 2));

          ctx.rotate((cw/2 + kerning) / (diameter / 2 - textHeight) * clockwise); // rotate half letter
        }
        return canvas;
      },

      _set: function(key:any, value:any) {
        switch(key) {
          case 'scaleX':
            this.fontSize *= value;
            this.diameter *= value;
            this.width *= value;
            this.scaleX = 1;
            if (this.width < 1) { this.width = 1; }
            break;

          case 'scaleY':
            this.height *= value;
            this.scaleY = 1;
            if (this.height < 1) { this.height = 1; }
            break;

          default:
            this.callSuper('_set', key, value);
            break;
        }
      },

      _render: function(ctx:any)
      {
        var canvas = this.getCircularText();
        this._trimCanvas(canvas);

        this.set('width', canvas.width);
        this.set('height', canvas.height);

        ctx.drawImage(canvas, -this.width / 2, -this.height / 2, this.width, this.height);

        this.setCoords();
      },

      toObject: function(propertiesToInclude:any) {
        return this.callSuper('toObject', ['text', 'diameter', 'kerning', 'flipped', 'fill', 'fontFamily', 'fontSize', 'fontWeight', 'fontStyle', 'strokeStyle', 'strokeWidth', 'styles'].concat(propertiesToInclude));
      }
    });

    fabric.TextCurved.fromObject = function(object:any, callback:any, forceAsync:any) {
       return fabric.Object._fromObject('TextCurved', object, callback, forceAsync, 'text-curved');
    };

    })(typeof fabric !== 'undefined' ? fabric : require('fabric').fabric);

}

undeline(canvas:any){
  let active:any= canvas.getActiveObjects()
  if(active){
    if(!active.underline){
      active.set({ underline:true})

    }else{
      active.set({ underline:false})
    }
  }
  canvas.requestRenderAll();

}

textfont=(event:any,canvas:any)=>{

  const myfont :any= new FontFace(event.name,`url(${event.url})`);
   myfont.load().then((loaded:any)=>{
     (document as any).fonts.add(loaded)
     let item:any[]= canvas.getActiveObjects();
     item.forEach(i=>{
  i.set({
  fontFamily:event.name
   });
 canvas.renderAll(i);
 canvas.requestRenderAll();
     })
   }).catch((err:any)=>{
     //console.log(err);
   })
   }


addCuveTex(fcanvas:any,text:any=null){
  this.curved()
  fcanvas.add(new fabric.TextCurved(text||'It feels so nice to be able to draw', {
		diameter: 360,
		fontSize: 32,
		fontFamily: 'Arial',
		left: 50,
		top: 50,
		fill: 'red'
	})).requestRenderAll();
}
}


