import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
const RECOM_SUP_CART='RECOM_SUP_CART'

@Injectable({
  providedIn: 'root'
})

export class CartService {
  cartUpdated = new Subject <any>()
  items = (():any => {
    const fieldValue = localStorage.getItem(RECOM_SUP_CART);
    return fieldValue === null
      ? []
      : JSON.parse(fieldValue);
  })();

  constructor() { }

async  saveItems(items:any){
   this.items.push(items)
   await localStorage.setItem(RECOM_SUP_CART,JSON.stringify(this.items));
   this.cartUpdated.next(this.items)
  }


  async deleteAll(){
    await localStorage.removeItem(RECOM_SUP_CART)
    this.items=[]
    this.cartUpdated.next(this.items)
  }

}
