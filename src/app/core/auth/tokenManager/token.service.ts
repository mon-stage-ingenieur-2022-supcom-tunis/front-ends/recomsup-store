import { Injectable } from '@angular/core';
const USER_RECOMP_SUP="RECOMSUPUSER_"
const TOKEN_ACCESS= 'ACCESS_TOKEN_RECOMSUP'
import { Subject } from 'rxjs';
import {v4 as uuidv4} from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
isAuth = new Subject<any>()
  constructor() { }
  saveUser(data:any){
    localStorage.setItem(USER_RECOMP_SUP,data)
  }

  saveToken(token:any){
    localStorage.setItem(TOKEN_ACCESS,token)
  }


  GetToken(){
    return localStorage.getItem(TOKEN_ACCESS)
  }

  GetUSER(){
    return localStorage.getItem(USER_RECOMP_SUP)
  }

  createOrgetUser(){
    let uuid = uuidv4();
    let user = {
      customerId:uuid+'_'+Date.now()
    }
   let User :any=  this.GetUSER()||null
   if(!User){
     User=user
     this.saveUser(JSON.stringify(User))
   }else{
    User=JSON.parse(User)
   }
   console.log(User)
   return User
  }


}
