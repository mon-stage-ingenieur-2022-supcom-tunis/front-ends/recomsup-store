import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'store',
    pathMatch: 'full'
  },
  {
    path: '',
    loadChildren: () => import('./editor/editor.module').then( m => m.EditorModule)
  },

  {
    path: 'store',
    loadChildren: () => import('./store/store.module').then( m => m.StoreModule)
  },

  {
    path: '***',
    loadChildren: () => import('./store/store.module').then( m => m.StoreModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{scrollPositionRestoration:"enabled"})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
