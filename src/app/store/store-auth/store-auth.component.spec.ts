import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreAuthComponent } from './store-auth.component';

describe('StoreAuthComponent', () => {
  let component: StoreAuthComponent;
  let fixture: ComponentFixture<StoreAuthComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoreAuthComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreAuthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
