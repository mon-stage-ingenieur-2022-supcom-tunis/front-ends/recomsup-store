import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreFootersComponent } from './store-footers.component';

describe('StoreFootersComponent', () => {
  let component: StoreFootersComponent;
  let fixture: ComponentFixture<StoreFootersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoreFootersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreFootersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
