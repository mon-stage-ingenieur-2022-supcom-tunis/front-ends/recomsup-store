import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreUserMessagesComponent } from './store-user-messages.component';

describe('StoreUserMessagesComponent', () => {
  let component: StoreUserMessagesComponent;
  let fixture: ComponentFixture<StoreUserMessagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoreUserMessagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreUserMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
