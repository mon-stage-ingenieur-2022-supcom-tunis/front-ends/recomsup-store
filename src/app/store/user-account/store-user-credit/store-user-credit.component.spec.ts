import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreUserCreditComponent } from './store-user-credit.component';

describe('StoreUserCreditComponent', () => {
  let component: StoreUserCreditComponent;
  let fixture: ComponentFixture<StoreUserCreditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoreUserCreditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreUserCreditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
