import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreUserAccountComponent } from './store-user-account.component';

describe('StoreUserAccountComponent', () => {
  let component: StoreUserAccountComponent;
  let fixture: ComponentFixture<StoreUserAccountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoreUserAccountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreUserAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
