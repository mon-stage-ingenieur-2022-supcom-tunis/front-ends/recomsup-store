import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreUserOrdersComponent } from './store-user-orders.component';

describe('StoreUserOrdersComponent', () => {
  let component: StoreUserOrdersComponent;
  let fixture: ComponentFixture<StoreUserOrdersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoreUserOrdersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreUserOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
