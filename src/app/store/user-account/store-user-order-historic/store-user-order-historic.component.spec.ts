import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreUserOrderHistoricComponent } from './store-user-order-historic.component';

describe('StoreUserOrderHistoricComponent', () => {
  let component: StoreUserOrderHistoricComponent;
  let fixture: ComponentFixture<StoreUserOrderHistoricComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoreUserOrderHistoricComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreUserOrderHistoricComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
