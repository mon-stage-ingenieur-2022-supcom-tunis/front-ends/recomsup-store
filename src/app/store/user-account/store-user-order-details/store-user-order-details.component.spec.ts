import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreUserOrderDetailsComponent } from './store-user-order-details.component';

describe('StoreUserOrderDetailsComponent', () => {
  let component: StoreUserOrderDetailsComponent;
  let fixture: ComponentFixture<StoreUserOrderDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoreUserOrderDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreUserOrderDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
