import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-store-user-order-details',
  templateUrl: './store-user-order-details.component.html',
  styleUrls: ['./store-user-order-details.component.scss']
})
export class StoreUserOrderDetailsComponent implements OnInit {
@Output() hist=new EventEmitter<any>()
  constructor() { }

  ngOnInit(): void {
  }


  showHist(){
   this.hist.emit(true)
  }

}
