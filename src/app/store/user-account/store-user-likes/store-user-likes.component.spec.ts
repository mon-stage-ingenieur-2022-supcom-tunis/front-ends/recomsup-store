import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreUserLikesComponent } from './store-user-likes.component';

describe('StoreUserLikesComponent', () => {
  let component: StoreUserLikesComponent;
  let fixture: ComponentFixture<StoreUserLikesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoreUserLikesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreUserLikesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
