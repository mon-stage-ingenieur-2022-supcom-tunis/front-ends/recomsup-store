import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreUserRecentSeenComponent } from './store-user-recent-seen.component';

describe('StoreUserRecentSeenComponent', () => {
  let component: StoreUserRecentSeenComponent;
  let fixture: ComponentFixture<StoreUserRecentSeenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoreUserRecentSeenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreUserRecentSeenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
