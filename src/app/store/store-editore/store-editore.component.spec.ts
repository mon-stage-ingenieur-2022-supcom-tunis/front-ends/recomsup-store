import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreEditoreComponent } from './store-editore.component';

describe('StoreEditoreComponent', () => {
  let component: StoreEditoreComponent;
  let fixture: ComponentFixture<StoreEditoreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoreEditoreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreEditoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
