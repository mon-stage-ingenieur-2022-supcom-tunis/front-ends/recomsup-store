import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CartService, EditorService, TokenService } from 'src/app/core';

declare var require:any;
var fabric = require("fabric").fabric
var swal = require("sweetalert2")

declare var FontFace:any;
var $ = require("jquery")
@Component({
  selector: 'app-store-editore',
  templateUrl: './store-editore.component.html',
  styleUrls: ['./store-editore.component.scss']
})
export class StoreEditoreComponent implements OnInit {
@Input() url:any
Canvas:any=[];
activeCanvas=0;
color:any=["#f44336", "#e91e63", "#9c27b0", "#673ab7", "#3f51b5", "#2196f3", "#03a9f4", "#00bcd4", "#009688", "#4caf50", "#8bc34a", "#cddc39", "#ffeb3b", "#ffc107", "#ff9800", "#ff5722", "#795548", "#607d8b"]
canvas:any;
text="Votre texte !!...."
User:any
fonts = [
  {name:"Flowerheart",url:"./assets/fonts/FlowerheartpersonaluseRegular-AL9e2.otf"},
  {name:"HussarBold",url:"./assets/fonts/HussarBoldWebEdition-xq5O.otf"},
  {name:'Branda',url:"./assets/fonts/Branda-yolq.ttf"},
  {name:"MarginDemo",url:"./assets/fonts/MarginDemo-7B6ZE.otf"},
  {name:"Kahlil",url:"./assets/fonts/Kahlil-YzP9L.ttf"},
  {name:"FastHand",url:"./assets/fonts/FastHand-lgBMV.ttf"},
  {name:"BigfatScript",url:"./assets/fonts/BigfatScript-jE96G.ttf"},
  {name:"Atlane",url:"./assets/fonts/Atlane-PK3r7.otf"},
  {name:"HidayatullahDemo",url:"./assets/fonts/Bismillahscript-4ByyY.ttf"},
  {name:"Robus",url:"./assets/fonts/HidayatullahDemo-mLp39.ttf"},
  {name:"Backslash",url:"./assets/fonts/Robus-BWqOd.otf"},
  {name:"ChristmasStory",url:"./assets/fonts/ChristmasStory-3zXXy.ttf"},
  {name:'Updock-Regular',url:'assets/fonts/Updock-Regular.ttf'}
 ];
  constructor(private editor:EditorService,private route:Router,private actRoute:ActivatedRoute,private cart:CartService,private token:TokenService) { }
  size =[{name:"XL",price:500},{name:"X",price:200},{name:"M",price:200}]
  printing=[{name:"DTF",price:522},{name:"Flexographie",price:500},{name:"Broderie",price:500}]
  menu:any
  color_:any="#23278a"

  ngOnInit(): void {
  //  window.onload = this.Scrolldown;
      // this.editor.curved()
      this.canvas =new fabric.Canvas('c',{
        hoverCursor: 'pointer',
        selection: true,
        fireRightClick: true,
        preserveObjectStacking: true,
        stateful:true,
        selectionBorderColor:'orange',
        stopContextMenu:false,
        height:200,
        width:200,

      })
    // console.log(screen) //screen
    this.editor.addText("votre texte ici",this.canvas)
      this.url=this.url =history.state
      // console.log(this.url.route)
       this.menu=this.actRoute.snapshot.params
      // this.createOrgetUser()
      this.User=this.token.createOrgetUser()

       if(this.url.canvas_top){
        let wrapperEl= this.canvas.wrapperEl
        let upperEl = this.canvas.upperCanvasEl
        let lowerEl = this.canvas.getElement()
        let style=`top:${this.url.canvas_top}px;border-radius:${this.url.canvas_border}%;left:${this.url.canvas_left}px;position:absolute;z-index: 300000;`
         wrapperEl.style = style
         upperEl.style=style
         lowerEl.style = style
         this.canvas.requestRenderAll()
         $('#testing').attr("style",`left:${this.url.image_left}px;top:${this.url.image_top}px;position:absolute;z-index:2000`)
         this.canvas.setWidth(this.url.width)
         this.canvas.setHeight(this.url.height)
         fabric.util.enlivenObjects(this.url.json.objects, (objects:any)=> {
          objects.forEach((o:any)=>{
            if(o.type="text"){
              if(o.path){
                o=this.editor.textPath(o)
              }
            }
            this.canvas.add(o);
          })
          this.canvas.renderAll();
          this.canvas.requestRenderAll();
        },"fabric");
       }
   /**
    *  this.canvas =new fabric.Canvas('c',{
      hoverCursor: 'pointer',
      selection: true,
      fireRightClick: true,
      preserveObjectStacking: true,
      stateful:true,
      selectionBorderColor:'orange',
      stopContextMenu:false,
    })
    this.canvas.wrapperEl.style='position:absolute'
    */
    //console.log(this.canvas)
   // this.addtext()

  }


  addtext(canvas:any=null,text:any=null){
    let message='HELLO'
    var text= new fabric.IText(text||message,{
      fill:"black",
      fontSize:25,
    });
    let item= this.canvas
    item.add(text)
    item.renderAll(text);
    item.centerObject(text);
    item.requestRenderAll(text);
  }


  font(item:any){
    let data= item.target.value;
    this.textfont(Object.assign({},{name:data.substr(0,data.indexOf("*")),url:data.substr(data.indexOf("*")+1,data.length-1)}),this.canvas)

   }

   Scrolldown() {
    $(document).ready(function($:any){
      if ( $(window).width() < 768 || window.Touch) {
          $('div').animate({
      scrollTop: $("#primary").offset().down
     }, 2000);
      }
});
   }

   textfont=(event:any,canvas:any)=>{

    const myfont :any= new FontFace(event.name,`url(${event.url})`);
     myfont.load().then((loaded:any)=>{
       (document as any).fonts.add(loaded)
       let item:any[]= canvas.getActiveObjects();
       item.forEach(i=>{
    i.set({
    fontFamily:event.name
     });
   canvas.renderAll(i);
   canvas.requestRenderAll();
       })
     }).catch((err:any)=>{
       //console.log(err);
     })
     }

addtocard(){
 // Sweat
 this.cart.saveItems({uri:this.url.uri,itemId:Date.now(),description:"enjoy your custom design !!!!",price:568}).then(()=>{
  swal.fire({
    icon:"success",
    text:"vous avez ajouter un article dans le panier",
    confirmButtonText: 'passer commande',
    cancelButtonText: 'non',
    showCancelButton: true,
    showCloseButton: true
  }).then((e:any)=>{
    if(e.isConfirmed){
      this.cart.deleteAll().then(()=>{
        swal.fire({
          icon:"success",
          text:"merci d'avoir passé une commande"
        })
       })
    }

  })
 })

}

OncolorPiked(){
  this.editor.color(this.canvas,this.color_)
}


makeItalic(){
  this.editor.italic(this.canvas)
}

makeBold(){
  this.editor.bold(this.canvas)
}

}
