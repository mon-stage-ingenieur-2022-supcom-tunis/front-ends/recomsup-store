import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-store-menu',
  templateUrl: './store-menu.component.html',
  styleUrls: ['./store-menu.component.scss']
})
export class StoreMenuComponent implements OnInit {
@Input() menu:any
  constructor() { }

  ngOnInit(): void {
  }

}
