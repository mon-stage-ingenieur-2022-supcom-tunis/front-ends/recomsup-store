import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-store-category',
  templateUrl: './store-category.component.html',
  styleUrls: ['./store-category.component.scss']
})
export class StoreCategoryComponent implements OnInit {
  products=[
    {url:"https://www.downloadclipart.net/large/polo-shirt-png-image.png"},
    {url:"https://pluspng.com/img-png/dry-fit-polo-shirts-in-dubai-uae-800.png"},
    {url:"https://assets.bigcartel.com/product_images/182651846/IMG_2957.JPG.jpeg?auto=format&fit=max&h=1000&w=1000"},
  {url:"https://www.fruit.com/dw/image/v2/ABAH_PRD/on/demandware.static/-/Sites-masterCatalog_FRUIT/default/dw08fd2156/images/hi-res/Fruit-of-the-Loom-Digital-Assets_58109.jpg?sw=1396&sh=1732&sm=fit&sfrm=png"},
    {url:"https://th.bing.com/th/id/OIP.jE-H8Qp9SqqPEFiwdrgE4gHaIU?pid=ImgDet&w=712&h=800&rs=1"},
    {
      url:"https://th.bing.com/th/id/OIP.AHxbR4okadSM2yL3nlWV1gHaHa?pid=ImgDet&w=612&h=612&rs=1"
    },
    {url:"https://th.bing.com/th/id/OIP.h85nWAy1akJ7sjtoxDVYsQHaHa?pid=ImgDet&w=600&h=600&rs=1"},

    {
    url:'https://th.bing.com/th/id/R.51d29a8db9ef93d9bb97030d0c841a7c?rik=LUR6aeM0nBh1Qg&riu=http%3a%2f%2fwww.hellopro.fr%2fimages%2fproduit-2%2f1%2f2%2f4%2fpolo-publicitaire-embassy-6143421.jpg&ehk=jI%2bfd73Muwx4hsPPqB3MkOTvgvyC8%2fe7VhBMKWG%2fH4s%3d&risl=&pid=ImgRaw&r=0'
  },{url:"https://th.bing.com/th/id/R.87008165ca9f1e413f4f9dc3f713592a?rik=7923xevYns3Z4w&riu=http%3a%2f%2fproductions-extreme.com%2fwp-content%2fuploads%2f2018%2f07%2ft-shirt-publicitaire.jpg&ehk=HkbX1JhhMjSGndA6dwyynLqWjJ6gsTVdI9JP%2bCTwkic%3d&risl=&pid=ImgRaw&r=0"},{
    url:"https://media.istockphoto.com/photos/polo-shirt-picture-id1222973610?k=6&m=1222973610&s=612x612&w=0&h=S-VsSn2uIv3MDrVVezY151xmh7f0IF1RKIa0CaTbSHc="
  },{
    url:"https://www.sevenstore.com/images/products/large/4055157.jpg"
  },{
    url:"https://th.bing.com/th/id/OIP.4Bx6uDlGGGMhQg1TAkeqagHaHC?pid=ImgDet&rs=1"
  },
  {url:"https://th.bing.com/th/id/OIP.1VJ8YFf8B079pnN1RdgyDQHaJ_?pid=ImgDet&w=1360&h=1836&rs=1"},
  {url:"https://media.missguided.com/i/missguided/Y9205565_03?fmt=jpeg&fmt.jpeg.interlaced=true&$product-page__main--2x$"}
  ,{
    url:"https://th.bing.com/th/id/R.3f13010dd9ba738e57cceb259936cb9a?rik=1T7%2bJ9S39ohndQ&riu=http%3a%2f%2fwww.pngplay.com%2fwp-content%2fuploads%2f3%2fStylo-PNG-Fond-De-Fichier.png&ehk=198kd889Pn8xbDoV9RDsVdZHl%2bS0cAxMO5L8rf66UvE%3d&risl=&pid=ImgRaw&r=0&sres=1&sresct=1"
  },
  {url:"https://cdn0.iconfinder.com/data/icons/pulse_pack_by_chromatix/443/stylo.png"},{
    url:"https://plateforme-epi.com/1168-superlarge_default_2x/t-shirt.jpg"
  },
  {url:"https://s3.gsxtr.com/i/p/t-shirt-jordan-hbr-jumpman-flight-t-shirt-black-gym-red-183161-1080s-1.jpg"},
  {url:"https://th.bing.com/th/id/OIP.bWwM3tTUbgGIEW3lTeesDgHaHa?pid=ImgDet&w=2000&h=2000&rs=1"},
  {url:'https://cdn-img-3.wanelo.com/p/b73/992/7b0/1dc2a4e99ec90ad540b71fd/x354-q80.jpg'},
  {url:'https://s3.amazonaws.com/images.ecwid.com/images/15688341/2175305856.jpg'},
  {url:"https://th.bing.com/th/id/R.03cf484cc6513680e0a3be495468fedf?rik=UDITVoLhL1f9xw&pid=ImgRaw&r=0"},
  {url:"https://purepng.com/public/uploads/large/red-t-shirt-jrf.png"},
  {url:"https://th.bing.com/th/id/OIP.jmBo1KgA3ejf4F5A9wgObwHaJc?pid=ImgDet&w=870&h=1110&rs=1"},
  {url:"https://www.pngarts.com/files/3/Womens-T-Shirt-PNG-Image-Background.png"},
  {url:"https://www.pngarts.com/files/3/Women-T-Shirt-Transparent-Background-PNG.png"}
  ]
  text="ishdisad"
  url="https://th.bing.com/th/id/R.03cf484cc6513680e0a3be495468fedf?rik=UDITVoLhL1f9xw&pid=ImgRaw&r=0";
  color:any=["#f44336", "#e91e63", "#9c27b0", "#673ab7", "#2196f3", "#03a9f4", "#00bcd4", "#009688", "#4caf50", "#8bc34a", "#cddc39", "#ffeb3b", "#ffc107", "#ff9800", "#ff5722", "#795548", "#607d8b"]
menu:any
  constructor(private router:Router,private actRoute: ActivatedRoute) { }
  baseRoute={name:"vetements",mode:"homme",product:"pull",item:"pulljaskl et bfoip"}

  ngOnInit(): void {
 // console.log(this.router.sn)
 //console.log()
 this.menu=this.actRoute.snapshot.params
// console.log(this.menu)
  }

  customize(url:any,route:any=null){

   // this.router.navigateByUrl("/store/editor",{state:{url:url}})
    this.router.navigateByUrl("/store/editor/"+route.name+"/"+route.mode+"/"+route.product+"/"+route.item,{state:{uri:url,route:route}})


   // this.sendInteraction(url)
   }


}
