import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StoreAuthComponent } from './store-auth/store-auth.component';
import { StoreCartComponent } from './store-cart/store-cart.component';
import { StoreCategoryComponent } from './store-category/store-category.component';
import { StoreCheckOutComponent } from './store-check-out/store-check-out.component';
import { StoreEditoreComponent } from './store-editore/store-editore.component';
import { StoreLandingPageComponent } from './store-landing-page/store-landing-page.component';
import { StoreUserDashboardComponent } from './user-account/store-user-dashboard/store-user-dashboard.component';
const routes: Routes = [
  {
  path:'',
  component: StoreLandingPageComponent,
},
{
  path:'editor',
  component:StoreEditoreComponent,

},

{
  path:'editor/:name/:mode/:product/:item',
  component:StoreEditoreComponent,

},
{
  path:'editor/:name/:product/:item',
  component:StoreEditoreComponent,

},

 {
  path: 'customer',component:StoreUserDashboardComponent
 },
{
  path:'cart',
  component:StoreCartComponent,

},
{
  path:'store-auth',
  component:StoreAuthComponent,

},

{
  path:"store-check-out",
  component:StoreCheckOutComponent
},
{
  path:"store-category",
  component:StoreCategoryComponent
},
{
  path:"store-category/:name",
  component:StoreCategoryComponent
},
{
  path:"store-category/:name/:mode/:product",
  component:StoreCategoryComponent
},
{
  path:"store-category/:name/:product",
  component:StoreCategoryComponent
},
{
  path:"store-category/:name/:product/:item",
  component:StoreEditoreComponent
}

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StoreRoutingModule { }
