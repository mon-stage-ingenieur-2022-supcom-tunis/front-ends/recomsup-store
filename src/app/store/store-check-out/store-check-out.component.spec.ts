import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreCheckOutComponent } from './store-check-out.component';

describe('StoreCheckOutComponent', () => {
  let component: StoreCheckOutComponent;
  let fixture: ComponentFixture<StoreCheckOutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoreCheckOutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreCheckOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
