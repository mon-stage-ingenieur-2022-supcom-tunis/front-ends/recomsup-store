import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartService, TokenService } from 'src/app/core';
declare var require:any
var Swal= require("sweetalert2")

//import (ui)
@Component({
  selector: 'app-store-cart',
  templateUrl: './store-cart.component.html',
  styleUrls: ['./store-cart.component.scss']
})
export class StoreCartComponent implements OnInit {
url:any='https://th.bing.com/th/id/OIP.h85nWAy1akJ7sjtoxDVYsQHaHa?pid=ImgDet&w=600&h=600&rs=1'
items:any=[]
total=0
hasItem=false;
User:any
  constructor(private route:Router,private cart:CartService,private token:TokenService) { }
  ngOnInit(): void {
    this.items=this.cart.items
    this.items.forEach((item:any)=>{
      this.total=this.total+item.price
    })
    if(this.total>0){
      this.hasItem=true
    }
   this.User =this.token.createOrgetUser()
  }

  checkOut(){
    //this.route.navigateByUrl("/store/store-check-out",{})
    this.cart.deleteAll().then(()=>{
      this.hasItem=false
      this.items=[]
      Swal.fire({
        icon:"success",
        text:"merci d'avoir passé une commande"
      })
    })

  }


}
