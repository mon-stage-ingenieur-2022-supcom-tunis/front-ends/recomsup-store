import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreHeadComponent } from './store-head.component';

describe('StoreHeadComponent', () => {
  let component: StoreHeadComponent;
  let fixture: ComponentFixture<StoreHeadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoreHeadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreHeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
