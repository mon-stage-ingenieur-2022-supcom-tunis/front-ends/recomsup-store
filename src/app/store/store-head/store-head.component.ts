import { Component, OnInit } from '@angular/core';
import { CartService, TokenService } from 'src/app/core';

@Component({
  selector: 'app-store-head',
  templateUrl: './store-head.component.html',
  styleUrls: ['./store-head.component.scss']
})
export class StoreHeadComponent implements OnInit {
  constructor(private auth:TokenService,private cart:CartService,private token:TokenService) { }
isauth=false
name="dave"
cartLength =0
  ngOnInit(): void {
    this.cartLength=this.cart.items.length
    this.auth.isAuth.subscribe(res=>{
      if(res){
       this.isauth=true
       this.name=res.name||this.name
      }
    })
    this.cart.cartUpdated.subscribe(res=>{
      if(res){
        this.cartLength=res.length||0
      }
    })
  }




}
