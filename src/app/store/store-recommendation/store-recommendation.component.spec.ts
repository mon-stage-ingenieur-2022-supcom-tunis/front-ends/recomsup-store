import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreRecommendationComponent } from './store-recommendation.component';

describe('StoreRecommendationComponent', () => {
  let component: StoreRecommendationComponent;
  let fixture: ComponentFixture<StoreRecommendationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoreRecommendationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreRecommendationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
