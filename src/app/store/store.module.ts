import { NgModule ,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreLandingPageComponent } from './store-landing-page/store-landing-page.component';
import { ShadeModule } from 'ngx-color';
import { StoreRoutingModule } from './store-routing.module';
import { StoreEditoreComponent } from './store-editore/store-editore.component';
import { ColorCircleModule } from 'ngx-color/circle';
import { StoreHeadComponent } from './store-head/store-head.component';
import { StoreMenuComponent } from './store-menu/store-menu.component';
import { StoreCartComponent } from './store-cart/store-cart.component';
import { StoreRecommendationComponent } from './store-recommendation/store-recommendation.component';
import { StoreAuthComponent } from './store-auth/store-auth.component';
import { StoreFootersComponent } from './store-footers/store-footers.component';
import { StoreUserAccountComponent } from './user-account/store-user-account/store-user-account.component';
import { StoreUserOrdersComponent } from './user-account/store-user-orders/store-user-orders.component';
import { StoreUserMessagesComponent } from './user-account/store-user-messages/store-user-messages.component';
import { StoreUserLikesComponent } from './user-account/store-user-likes/store-user-likes.component';
import { StoreUserRecentSeenComponent } from './user-account/store-user-recent-seen/store-user-recent-seen.component';
import { StoreUserCreditComponent } from './user-account/store-user-credit/store-user-credit.component';
import { StoreUserDashboardComponent } from './user-account/store-user-dashboard/store-user-dashboard.component';
import { StoreUserOrderDetailsComponent } from './user-account/store-user-order-details/store-user-order-details.component';
import { StoreUserOrderHistoricComponent } from './user-account/store-user-order-historic/store-user-order-historic.component';
import { StoreCheckOutComponent } from './store-check-out/store-check-out.component';
import { StoreCategoryComponent } from './store-category/store-category.component'; // <color-circle></color-circle>
import { FormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    StoreLandingPageComponent,
    StoreEditoreComponent,
    StoreHeadComponent,
    StoreMenuComponent,
    StoreCartComponent,
    StoreRecommendationComponent,
    StoreAuthComponent,
    StoreFootersComponent,
    StoreUserAccountComponent,
    StoreUserOrdersComponent,
    StoreUserMessagesComponent,
    StoreUserLikesComponent,
    StoreUserRecentSeenComponent,
    StoreUserCreditComponent,
    StoreUserDashboardComponent,
    StoreUserOrderDetailsComponent,
    StoreUserOrderHistoricComponent,
    StoreCheckOutComponent,
    StoreCategoryComponent
  ],
  imports: [
    CommonModule,
    ShadeModule,
    StoreRoutingModule,
    ShadeModule,
    ColorCircleModule,
    FormsModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class StoreModule { }
