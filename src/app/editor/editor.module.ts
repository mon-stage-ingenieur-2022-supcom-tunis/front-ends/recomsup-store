import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditorRoutingModule } from './editor-routing.module';
import { AladinEditorComponent } from './aladin-editor/aladin-editor.component';
import { AladinAppComponent } from './aladin-app/aladin-app.component';
import { CustomizerComponent } from './customizer/customizer.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AladinEditorComponent,
    AladinAppComponent,
    CustomizerComponent
  ],
  imports: [
    CommonModule,
    EditorRoutingModule,
    FormsModule
  ]
})
export class EditorModule { }
