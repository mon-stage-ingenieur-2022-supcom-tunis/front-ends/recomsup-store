import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AladinEditorComponent } from './aladin-editor/aladin-editor.component';


const routes: Routes = [
  {
  path:'aladin-editor',
  component:AladinEditorComponent
},



];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditorRoutingModule { }
