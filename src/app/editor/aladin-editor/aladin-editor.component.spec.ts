import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AladinEditorComponent } from './aladin-editor.component';

describe('AladinEditorComponent', () => {
  let component: AladinEditorComponent;
  let fixture: ComponentFixture<AladinEditorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AladinEditorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AladinEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
