import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AladinAppComponent } from './aladin-app.component';

describe('AladinAppComponent', () => {
  let component: AladinAppComponent;
  let fixture: ComponentFixture<AladinAppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AladinAppComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AladinAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
