import { Component, Input, OnInit } from '@angular/core';
import { EditorService } from 'src/app/core';
declare var require:any
const fabric = require('fabric').fabric
const $=require("jquery")

@Component({
  selector: 'app-aladin-app',
  templateUrl: './aladin-app.component.html',
  styleUrls: ['./aladin-app.component.scss']
})
export class AladinAppComponent implements OnInit {
@Input() url:any
canvas:any;
  constructor(private editor:EditorService) { }

  ngOnInit(): void {
   // this.editor.curved()
    this.canvas =new fabric.Canvas('test-1',{
      hoverCursor: 'pointer',
      selection: true,
      fireRightClick: true,
      preserveObjectStacking: true,
      stateful:true,
      selectionBorderColor:'orange',
      stopContextMenu:false,
      height:300,
      width:300
    })
  // console.log(screen) //screen
  
    let wrapperEl= this.canvas.wrapperEl
    let upperEl = this.canvas.upperCanvasEl
    let lowerEl = this.canvas.getElement()
    let style=`top:${this.url.canvas_top}px;border-radius:${this.url.canvas_border}%;left:${this.url.canvas_left}px;position:absolute;z-index: 300000;`
     wrapperEl.style = style
     upperEl.style=style
     lowerEl.style = style
     this.canvas.requestRenderAll()
     $('#testing').attr("style",`left:${this.url.image_left}px;top:${this.url.image_top}px;position:absolute;z-index:2000`)
     this.canvas.setWidth(this.url.width)
     this.canvas.setHeight(this.url.height)
     fabric.util.enlivenObjects(this.url.json.objects, (objects:any)=> {
      objects.forEach((o:any)=>{
        if(o.type="text"){
          if(o.path){
            o=this.editor.textPath(o)
          }
        }
        this.canvas.add(o);
      })
      this.canvas.renderAll();
      this.canvas.requestRenderAll();
    },"fabric");


  }

}
